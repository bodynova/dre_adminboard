<?php

$sLangName = "Deutsch";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                   => 'UTF-8',

    'MXDASHBOARD'                       => 'Dashboard',
    'MXDASHBOARDORDERDATA'              => 'Bestellungen',
    'MXDASHBOARDORDERDATA_MONTH'        => 'Monat',
    'MXDASHBOARDORDERDATA_YEAR'         => 'Jahr',
    'MXDASHBOARDORDERDATA_PERIOD'       => 'Zeitspanne',
    'MXUSEDPAYMENT'                     => 'Verwendete Bezahlarten',
    'MXTOPSELLERCATS'                   => 'Topseller Kategorien',
    'MXARTICLESTOPONLYACTIVE'           => 'Artikel verkauft TOP 10 - nur aktiv',
    'MXARTICLESTOPALL'                  => 'Artikel verkauft TOP 10 - alle',
    'MXSELLCOUNT'                       => 'Anzahl Verkäufe',
    'MXARTICLE'                         => 'Artikel',
    'MXORDERVALUE'                      => 'Bestellwert',
    'MXORDERVALUES'                     => 'Bestellwerte',
    'MXTYPE'                            => 'Typ',
    'MXBIGGEST'                         => 'Höchster',
    'MXAVG'                             => 'Durchschnittlicher',
    'MXSMALLEST'                        => 'Niedrigster',
    'MXALL'                             => 'Insgesammt',
    'MXTODAY'                           => 'heute',
    'MXCOUNT'                           => 'Anzahl',
    'MXORDERSTODAY'                     => 'Bestellungen heute',
    'MXORDERSTHISMONTH'                 => 'Bestellungen aktueller Monat',
    'MXORDERSCOMPLETE'                  => 'Bestellungen gesammt',
    'MXUSERACCOUNT'                     => 'Kunden Konten Typen',
    'MXNEWSLETTER'                      => 'Kunden Newsletter Anmeldungen',
    'MXORDERSQUALITY'                   => 'Stornoanteil',
    'MXORDERSTATES'                     => 'Bestellstatus',
    'MXARTICLEINFOS'                    => 'Artikelinfos',
    'MXARTICLEALL'                      => 'Gesammt',
    'MXARTICLEPARENTS'                  => 'Vaterartikel',
    'MXARTICLEVARIANTS'                 => 'Varianten',
    'MXARTICLEACTIVE'                   => 'Nur aktive',

    'MXINFO'                            => 'BODYNOVA',

    /* WELTEN */
    'MXWELTENTITEL'                     => 'Best Verkaufte Artikel Pro Welt',
    'MXARTIKELNUMMER'                   => 'ART.NO.',
    'MXTITEL'                           => 'TITEL',
    'MXUMSATZ'                          => 'UMSATZ',
    'MXANZAHL'                          => 'Anzahl',
    'MXWELT1'                           => 'MASSAGE & PRAXISBEDARF',
    'MXWELT2'                           => 'WELLNESS & GESUNDHEIT',
    'MXWELT3'                           => 'YOGA & PILATES',
    'MXWELT4'                           => 'MEDITATION',
    'MXWELT5'                           => 'ANGEBOTE',
    'MXANZAHLARTIKEL'                   => 'GESAMTARTIKELVÄTER',



    /* SETTINGS */
    'SHOP_MODULE_GROUP_dre_AdminBoard'                => 'Dashboard Anzeigeeinstellungen',
    'SHOP_MODULE_dre_AdminBoard_orders'               => 'Bestellungen',
    'SHOP_MODULE_dre_AdminBoard_payments'             => 'Bezahlarten',
    'SHOP_MODULE_dre_AdminBoard_topsellerCategories'  => 'Topseller Kategorien',
    'SHOP_MODULE_dre_AdminBoard_ordersStorno'         => 'Stornoanteil',
    'SHOP_MODULE_dre_AdminBoard_ordersState'          => 'Bestellstatus',
    'SHOP_MODULE_dre_AdminBoard_orderValues'          => 'Bestellwerte',
    'SHOP_MODULE_dre_AdminBoard_customerAcountTypes'  => 'Kunden Konten Typen',
    'SHOP_MODULE_dre_AdminBoard_customerBought'       => 'Kundenanteil kaufte',
    'SHOP_MODULE_dre_AdminBoard_customerNewsletters'  => 'Kunden Newsletter anmeldungen',
    'SHOP_MODULE_dre_AdminBoard_articleInfos'         => 'Artikel Informationen',
    'SHOP_MODULE_dre_AdminBoard_articleTopActive'     => 'Artikel Topseller Aktiv',
    'SHOP_MODULE_dre_AdminBoard_articleTopAll'        => 'Artikel Topseller Alle',
);


