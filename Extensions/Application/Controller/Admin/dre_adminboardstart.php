<?php

namespace Bender\dre_AdminBoard\Extensions\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;

class dre_adminboardstart extends dre_adminboardstart_parent
{
    protected $_sThisTemplate = 'dre_start.tpl';

    public function render(){
        $this->_aViewData['dafusolo'] = 'test';
        return parent::render();
    }
}