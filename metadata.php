<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'dre_adminboard',
    'title'       => [
        'en' => '<img src="../modules/bender/dre_adminboard/out/img/favicon.ico" title="Bodynova Admin Rechte Modul">odynova Admin dashboard',
        'de' => '<img src="../modules/bender/dre_adminboard/out/img/favicon.ico" title="Bodynova Admin Rechte Modul">odynova Admin Dashboard',
    ],
    'description' => [
        'en' => 'Shows shop statistics at your OXID eShop Admin startpage',
        'de' => 'Anzeige von Shopstatistiken anstatt der standard OXID eShop Admin Startseite',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'support@bodynova.de',
    'controllers'       => [

        'dre_admindashboard'                        =>
            \Bender\dre_AdminBoard\Extensions\Application\Controller\Admin\dre_admindashboard::class,

        'dre_admincontroller'   =>
            //\Bender\dre_AdminBoard\Application\Controller\Admin\dre_admincontroller::class,
            \Bender\dre_AdminBoard\Application\Controller\Admin\dre_admincontroller::class,

    ],
    'extend'      => [
        \OxidEsales\Eshop\Application\Controller\Admin\AdminStart::class            =>
            \Bender\dre_AdminBoard\Extensions\Application\Controller\Admin\dre_adminboardstart::class,
        \OxidEsales\Eshop\Application\Controller\Admin\NavigationController::class  =>
            \Bender\dre_AdminBoard\Extensions\Application\Controller\Admin\dre_admindashboard::class
    ],
    'templates'   => [
        'dre_start.tpl'                   => 'bender/dre_adminboard/Application/views/admin/tpl/dre_start.tpl',
        'dre_adminboard.tpl'              => 'bender/dre_adminboard/Application/views/admin/tpl/dre_adminboard.tpl',
        'dre_orderchart.tpl'              => 'bender/dre_adminboard/Application/views/admin/tpl/dre_orderchart.tpl',
        'dre_startmenu.tpl'               => 'bender/dre_adminboard/Application/views/admin/tpl/dre_startmenu.tpl',
    ],
    'blocks'      => [],
    'settings'    => [
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_orders',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_payments',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_topsellerCategories',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_ordersStorno',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_ordersState',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_orderValues',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_customerAcountTypes',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_customerBought',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_customerNewsletters',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_articleInfos',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_articleTopActive',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_articleTopAll',
            'type'  => 'bool',
            'value' => '0'
        ],
    ],
];